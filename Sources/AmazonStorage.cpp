#include "AmazonStorage.h"

namespace Amazon {
    const std::map<unsigned, Item> &Storage::getItems() const
    {
        return items_;
    }

    unsigned Storage::addItem(const std::string &name, double price, Type type)
    {
        currentId_++;
        items_[currentId_] = {name, price, type};
        return currentId_;
    }

    Storage::Storage()
            : currentId_(0)
    {
    }

    void Storage::removeItem(unsigned int id)
    {
        if(!items_.count(id))
            throw std::runtime_error("No item with id "+ std::to_string(id) + " in storage");
        else
            items_.erase(id);
    }


}




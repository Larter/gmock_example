#ifndef GMOCK_TRAINING_AMAZONFINDER_H
#define GMOCK_TRAINING_AMAZONFINDER_H

#include <functional>
#include <vector>
#include <string>
#include <memory>
#include "AmazonStorage.h"

namespace Amazon {

    struct ShortDescription
    {
        ShortDescription(const std::string&, double);

        const std::string name;

        void setPrice(double);

        double getPrice() const;

    private:
        double price_;
    };

    class AmazonFinder
    {
    private:
        const Storage &storage_;
    public:
        AmazonFinder(const Storage &);

        std::vector<double> findAllPricesOfSubname(const std::string &) const; // WhenSorted

        std::vector<double> findAllPricesOfSubnameSorted(const std::string &) const; //ContainerEq, ElementsAre

        std::vector<ShortDescription> findAllNamesAndPricesOfSubname(
                const std::string &) const; // Pointwise with AllOf and Field+ property

        std::unique_ptr<double> findLowestPriceOfName(const std::string &) const;

        std::vector<std::string> findNamesWithPricesInRange(double priceBottom, double priceTop); //Parametric tests

        std::vector<ShortDescription> getAnyMatching3WithOneSelected(const std::string &, std::function<bool(
                const std::string &)>) const;//Contains, Each

        std::string getCommonCase(const std::string &input) const;
    };

}
#endif //GMOCK_TRAINING_AMAZONFINDER_H

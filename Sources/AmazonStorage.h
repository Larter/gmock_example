#ifndef GMOCK_TRAINING_AMAZONSTORAGE_H
#define GMOCK_TRAINING_AMAZONSTORAGE_H

#include <map>

namespace Amazon {

    enum class Type
    {
        Book,
        Reader,
        Game
    };

    struct Item
    {
        std::string name;
        double price;
        Type type;
    };

    using ItemsMap = std::map<unsigned, Item>;

    class Storage
    {
        ItemsMap items_;
        unsigned currentId_;

    public:
        Storage();
        const std::map<unsigned, Item> &getItems() const;
        unsigned addItem(const std::string &name, double price, Type type);

        void removeItem(unsigned int id);
    };
}
#endif //GMOCK_TRAINING_AMAZONSTORAGE_H

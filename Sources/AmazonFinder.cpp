#include <algorithm>
#include "AmazonFinder.h"

namespace Amazon
{

    void ShortDescription::setPrice(double p)
    {
        price_ = p;
    }

    double ShortDescription::getPrice() const
    {
        return price_;
    }

    ShortDescription::ShortDescription(const std::string & input, double price)
        :name(input), price_(price)
    {}


    std::vector<double> AmazonFinder::findAllPricesOfSubname(const std::string & input) const
    {
        std::vector<double> result;
        std::string inputLower = getCommonCase(input);

        std::string itemName;
        for (const auto item : storage_.getItems())
            if (getCommonCase(item.second.name).find(inputLower) != std::string::npos)
                result.push_back(item.second.price);

        return result;
    }

    std::vector<double> AmazonFinder::findAllPricesOfSubnameSorted(const std::string & input) const
    {
        auto result = findAllPricesOfSubname(input);
        std::sort(result.begin(),result.end());
        return result;
    }

    std::vector<ShortDescription> AmazonFinder::findAllNamesAndPricesOfSubname(const std::string & input) const
    {

        std::vector<ShortDescription> result;
        std::string inputLower = getCommonCase(input);

        std::string itemName;
        for (const auto item : storage_.getItems())
            if (getCommonCase(item.second.name).find(inputLower) != std::string::npos)
                result.push_back(ShortDescription(item.second.name,item.second.price));
        return result;
    }

    std::string AmazonFinder::getCommonCase(const std::string &input) const
    {
        std::string inputLower = input;
        transform(inputLower.begin(), inputLower.end(), inputLower.begin(), tolower);
        return inputLower;
    }

    std::vector<ShortDescription> AmazonFinder::getAnyMatching3WithOneSelected(const std::string &, std::function<bool(
            const std::string &)>) const
    {
        return std::vector<ShortDescription>();
    }

    std::vector<std::string> AmazonFinder::findNamesWithPricesInRange(double priceBottom, double priceTop)
    {
        return std::vector<std::string>();
    }

    AmazonFinder::AmazonFinder(const Storage & s)
        :storage_(s)
    {}

    std::unique_ptr<double> AmazonFinder::findLowestPriceOfName(const std::string &) const
    {
        return std::unique_ptr<double>();
    }


}


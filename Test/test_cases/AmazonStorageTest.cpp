#include <memory>
#include "gmock/gmock.h"
#include "AmazonStorage.h"

using namespace Amazon;
using namespace testing;

MATCHER(ItemsEqual, "")
{
    auto itemId = std::get<0>(arg).first;
    auto expectedId = std::get<1>(arg).first;
    auto item = std::get<0>(arg).second;
    auto expectedItem = std::get<1>(arg).second;

    EXPECT_EQ(itemId, expectedId);
    EXPECT_EQ(item.price, expectedItem.price);
    EXPECT_EQ(item.name, expectedItem.name);
    EXPECT_EQ(item.type, expectedItem.type);
    return true;
}

namespace Amazon {

    void PrintTo(const Item &item, ::std::ostream *os)
    {
        *os << "{ name Enchanced: " << item.name << ", price: " << item.price << " , type: " <<
        static_cast<int>(item.type);
    }
}

TEST(AmazonStorageTest, shallAddSingleItemAndReturnIt)
{
    Storage storage;
    storage.addItem("Item", 0.3, Type::Book);

    ItemsMap expected =
            {
                    {1, {"Item", 0.3, Type::Book}}
            };
    EXPECT_THAT(storage.getItems(), Pointwise(ItemsEqual(), expected));
}


TEST(AmazonStorageTest, shallManyItemsAndReturnIt)
{
    Storage storage;
    storage.addItem("Item", 0.3, Type::Book);
    storage.addItem("Item2", 0.4, Type::Reader);

    ItemsMap expected =
            {
                    {1, {"Item", 0.3, Type::Book}},
                    {2, {"Item2", 0.4, Type::Reader}}
            };
    EXPECT_THAT(storage.getItems(), Pointwise(ItemsEqual(), expected));
}


TEST(AmazonStorageTest, removeItemShallRemoveExistingItem)
{
    Storage storage;
    storage.addItem("Item", 0.3, Type::Book);
    storage.addItem("Item2", 0.4, Type::Reader);

    EXPECT_NO_THROW(storage.removeItem(1));
    ItemsMap expectedAfterRemove =
            {
                    {2, {"Item2", 0.4, Type::Reader}}
            };
    EXPECT_THAT(storage.getItems(), Pointwise(ItemsEqual(), expectedAfterRemove));
}


TEST(AmazonStorageTest, removeItemShallThrowWhenRemovingNotExistingItem)
{
    Storage storage;
    storage.addItem("Item", 0.3, Type::Book);
    storage.addItem("Item2", 0.4, Type::Reader);

    EXPECT_THROW(storage.removeItem(3), std::runtime_error);
    ItemsMap expected =
            {
                    {1, {"Item", 0.3, Type::Book}},
                    {2, {"Item2", 0.4, Type::Reader}}
            };
    EXPECT_THAT(storage.getItems(), Pointwise(ItemsEqual(), expected));
}

TEST(AmazonStorageTest, removeItemShallThrowWhenAlreadyRemovedItem)
{
    Storage storage;
    storage.addItem("Item", 0.3, Type::Book);
    storage.addItem("Item2", 0.4, Type::Reader);

    EXPECT_NO_THROW(storage.removeItem(1));
    EXPECT_THROW(storage.removeItem(1), std::runtime_error);

    ItemsMap expected =
            {
                    {2, {"Item2", 0.4, Type::Reader}}
            };
    EXPECT_THAT(storage.getItems(), Pointwise(ItemsEqual(), expected));
}

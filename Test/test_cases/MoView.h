//
// Created by sosnicki on 12/19/2016.
//

#ifndef GMOCK_TRAINING_MOVIEW_H
#define GMOCK_TRAINING_MOVIEW_H

namespace detail{

const struct tag_static_cast_t {} tag_static_cast {};
const struct tag_dynamic_cast_t {} tag_dynamic_cast {};

struct MoViewBase
{
    MoViewBase() : name_(nullptr), data_(nullptr) {}

    MoViewBase(const std::string* name, void* data) : name_(name), data_(data) {}

    const std::string& name() const
    {
        assert(name_);
        return *name_;
    }

    const std::string* namePtr() const { return name_; }

    void* get() { return data_; }

    const void* get() const { return data_; }

    explicit operator bool() const { return name_ && data_; }

private:
    const std::string* name_;
    void* data_;
};

}   // namespace detail

#define MOVIEW_CTOR(Mo) \
    MoView() {}\
\
    MoView(const std::string* name, Mo* data) :\
        Base(name, data)\
    { }\
\
    template <typename U>\
    MoView(const MoView<U>& other, detail::tag_static_cast_t) :\
        Base(other.namePtr(), static_cast<Mo*>(const_cast<U*>(other.get())))\
    {}\
\
    template <typename U>\
    MoView(const MoView<U>& other, detail::tag_dynamic_cast_t) :\
        Base(other.namePtr(), dynamic_cast<Mo*>(const_cast<U*>(other.get())))\
    {}

template <typename Mo>
struct MoView : public detail::MoViewBase
{
private:
    using Base = detail::MoViewBase;

public:
    using element_type = Mo;
    MOVIEW_CTOR(Mo)

    template <typename U>
    MoView(
            const MoView<U>& other,
            typename std::enable_if<std::is_convertible<U, Mo>::value, int>::type = 0) :
            Base(other.namePtr(), const_cast<U*>(other.get()))
    {}

    Mo& mo()
    {
        auto* ptr = get();
        assert(ptr);
        return *ptr;
    }

    const Mo& mo() const
    {
        const auto* ptr = get();
        assert(ptr);
        return *ptr;
    }

    Mo* get()
    {
        return static_cast<Mo*>(MoViewBase::get());
    }

    const Mo* get() const
    {
        return static_cast<const Mo*>(MoViewBase::get());
    }

    Mo& operator*()
    {
        return mo();
    }

    const Mo& operator*() const
    {
        return mo();
    }

    Mo* operator->()
    {
        return get();
    }

    const Mo* operator->() const
    {
        return get();
    }

    static const char* className()
    {
        return Mo::className();
    };
};

template <>
struct MoView<void> : public detail::MoViewBase
{
private:
    using Base = detail::MoViewBase;

public:
    MOVIEW_CTOR(void)

    template <typename U>
    MoView(const MoView<U>& other) :
            MoView(other.namePtr(), const_cast<U*>(other.get()))
    {}
};

#undef MOVIEW_CTOR

inline bool operator==(const detail::MoViewBase& lhs, const detail::MoViewBase& rhs)
{
    return lhs.get() == rhs.get();
}

inline bool operator!=(const detail::MoViewBase& lhs, const detail::MoViewBase& rhs)
{
    return !(lhs == rhs);
}

inline bool operator<(const detail::MoViewBase& lhs, const detail::MoViewBase& rhs)
{
    return lhs.get() < rhs.get();
}

template <typename Out, typename In>
MoView<Out> static_mo_cast(const MoView<In>& in)
{
    return MoView<Out>(in, detail::tag_static_cast);
}

template <typename Out, typename In>
MoView<Out> dynamic_mo_cast(const MoView<In>& in)
{
    return MoView<Out>(in, detail::tag_dynamic_cast);
}

template <typename T>
inline std::string basicToString(const MoView<T>& mo)
{
    return mo.name();
}


#endif //GMOCK_TRAINING_MOVIEW_H

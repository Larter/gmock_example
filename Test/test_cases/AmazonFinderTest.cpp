#include "gmock/gmock.h"
#include "AmazonFinder.h"

using namespace testing;
using namespace Amazon;


namespace Amazon
{
    void PrintTo(const ShortDescription& d, ::std::ostream* os) {
        *os<< "{ name: "<<d.name <<" price: "<< d.getPrice()<<"}";
    }
}

class AmazonFinderTest : public Test
{
protected:
    Storage storage;
    AmazonFinder finder;

    AmazonFinderTest(): finder(storage) { }

    auto shortDescriptionMatcher(std::string name, double price)
    {
        return AllOf(Field(&ShortDescription::name, name), Property(&ShortDescription::getPrice, price));
    };
};


class AmazonFinder_findAllPricesOfSubnameShall : public AmazonFinderTest { };

TEST_F(AmazonFinder_findAllPricesOfSubnameShall, returnEmptyVectorWhenNoItemsAreInStorage)
{
    EXPECT_THAT(finder.findAllPricesOfSubname(""), IsEmpty());
}

TEST_F(AmazonFinder_findAllPricesOfSubnameShall, returnSingleItemThatMatches)
{
    storage.addItem("Item", 0.3, Type::Book);
    EXPECT_THAT(finder.findAllPricesOfSubname("tem"), UnorderedElementsAre(0.3));
}

TEST_F(AmazonFinder_findAllPricesOfSubnameShall, returnSingleItemThatMatchesWithOtherCase)
{
    storage.addItem("Some ITEM", 6, Type::Reader);
    EXPECT_THAT(finder.findAllPricesOfSubname("tem"), UnorderedElementsAre(6));
}

TEST_F(AmazonFinder_findAllPricesOfSubnameShall, returnEmptyVectorIfItemDoNotMatch)
{
    storage.addItem("Item", 0.3, Type::Book);
    EXPECT_THAT(finder.findAllPricesOfSubname("something"), IsEmpty());
}

TEST_F(AmazonFinder_findAllPricesOfSubnameShall, workWithMultipleItems)
{
    storage.addItem("Something", 6, Type::Reader);
    storage.addItem("Broken item", 2, Type::Book);
    storage.addItem("Damaged ITEM", 8, Type::Game);
    storage.addItem("Nothing special", 4, Type::Book);
    storage.addItem("Item ITeM", 2, Type::Reader);
    EXPECT_THAT(finder.findAllPricesOfSubname("item"), UnorderedElementsAre(2, 8, 2));
}

class AmazonFinder_findAllPricesOfSubnameSortedShall : public AmazonFinderTest { };

TEST_F(AmazonFinder_findAllPricesOfSubnameSortedShall,returnEmptyVectorWhenNoItemsAreInStorage)
{
    EXPECT_THAT(finder.findAllPricesOfSubname(""), IsEmpty());
}

TEST_F(AmazonFinder_findAllPricesOfSubnameSortedShall, workWithMultipleItems)
{
    storage.addItem("Something", 6, Type::Reader);
    storage.addItem("Broken item", 2, Type::Book);
    storage.addItem("Damaged ITEM", 8, Type::Game);
    storage.addItem("Nothing special", 4, Type::Book);
    storage.addItem("Item ITeM", 2, Type::Reader);
    EXPECT_THAT(finder.findAllPricesOfSubnameSorted("item"), ElementsAre(2, 2, 8));
}

class AmazonFinder_findAllNamesAndPricesOfSubnameShall : public AmazonFinderTest { };

TEST_F(AmazonFinder_findAllNamesAndPricesOfSubnameShall, returnEmptyVectorWhenNoItemsAreInStorage)
{
    EXPECT_THAT(finder.findAllNamesAndPricesOfSubname(""), IsEmpty());
}

TEST_F(AmazonFinder_findAllNamesAndPricesOfSubnameShall, returnEmptyVectorWhenNoMatchingItemsAreInStorage)
{
    storage.addItem("b", 6, Type::Reader);
    EXPECT_THAT(finder.findAllNamesAndPricesOfSubname("e"), IsEmpty());
}

TEST_F(AmazonFinder_findAllNamesAndPricesOfSubnameShall, returnMatchingItem)
{
    storage.addItem("cNaMeh", 6, Type::Reader);
    EXPECT_THAT(finder.findAllNamesAndPricesOfSubname("name"),ElementsAre(shortDescriptionMatcher("cNaMeh", 6)));
}

TEST_F(AmazonFinder_findAllNamesAndPricesOfSubnameShall, returnMatchingItems)
{
    storage.addItem("Something", 6, Type::Reader);
    storage.addItem("Broken item", 2, Type::Book);
    storage.addItem("Damaged ITEM", 8, Type::Game);
    storage.addItem("Nothing special", 4, Type::Book);
    storage.addItem("Item ITeM", 2, Type::Reader);

    EXPECT_THAT(finder.findAllNamesAndPricesOfSubname("item"),
                UnorderedElementsAre(
                        shortDescriptionMatcher("Damaged ITEM", 8),
                        shortDescriptionMatcher("Broken item", 2),
                        shortDescriptionMatcher("Item ITeM", 2)
                ));
}



struct ExStruct
{
    int a;
};



TEST(Example, PointeeEx)
{
    std::shared_ptr<int> empty_ptr;
    std::unique_ptr<int> i = std::make_unique<int>(2);
    EXPECT_THAT(empty_ptr, Pointee(1));
    EXPECT_THAT(i, Pointee(1));

    ExStruct* s ;/*= new ExStruct();
    s->a = 3;*/
    EXPECT_THAT(s, Field(&ExStruct::a, 2 ));
}


TEST(Example, SizeIsIsEmpty)
{
    std::vector<int> l{2};

    std::cout<<std::endl<<"EXPECT_TRUE/EQ "<<std::endl;
    EXPECT_TRUE(l.empty());
    EXPECT_EQ(l.size(), 2);

    std::cout<<std::endl<<"EXPECT_THAT "<<std::endl;

    EXPECT_THAT(l, IsEmpty());
    EXPECT_THAT(l, SizeIs(2));
}

TEST(Example, Ex_ContainerEq)
{
    {
        std::vector<int> l{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16 ,17, 18, 19, 20, 21, 22, 24, 25};
        std::vector<int> r{1, 2, 3, 4, 5, 6, 7, 8, 8, 10, 11, 12, 13, 14, 15, 16 ,17, 18, 19, 21, 22, 24, 25, 25};

        std::cout << "EXPECT_EQ :" << std::endl;
        EXPECT_EQ(l, r);

        std::cout << std::endl << "ContainerEq:" << std::endl;
        EXPECT_THAT(r, ContainerEq(l));
    }

    {
        std::vector<std::vector<std::vector<int>>> l{{{1},{1,2}}, {{2}}, {{1,3},{2,1}}};
        std::vector<std::vector<std::vector<int>>> r{{{1},{2,2}}, {{2}}, {{1,3},{0,1}}};

        std::cout << "EXPECT_EQ :" << std::endl;
        EXPECT_EQ(l, r);

        std::cout << std::endl << "ContainerEq:" << std::endl;
        EXPECT_THAT(l, ContainerEq(r));
    }

    {
        std::map<std::string, int> l{{"a", 1},{"b",2},{"c", 3}, {"d", 2}};
        std::map<std::string, int> r{{"a", 1},{"b",2},{"c", 4}};

        std::cout << "EXPECT_EQ :" << std::endl;
        EXPECT_EQ(l, r);

        std::cout << std::endl << "ContainerEq:" << std::endl;
        EXPECT_THAT(l, ContainerEq(r));
    }

}


TEST(Example, Ex_WhenSorted)
{
    std::vector<int> l{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 24, 25};
    std::vector<int> r{24, 6, 7, 13,  25, 5, 2, 3, 8, 8, 25, 16, 18, 14, 15, 11, 17, 12, 19, 21, 22, 1, 10, 4};

    std::cout << "EXPECT_EQ :" << std::endl;
    std::sort(r.begin(), r.end());
    EXPECT_EQ(l, r);

    std::cout << std::endl << "WhenSorted:" << std::endl;
    EXPECT_THAT(r, WhenSorted(ContainerEq(l)));

    std::cout << std::endl << "UnorderedElementsAre:" << std::endl;
    EXPECT_THAT(r, UnorderedElementsAreArray(l));
}


struct MyStruct{
    int a;
    float b;
};


bool operator == (const MyStruct & l, const MyStruct & r)
{
    return l.a == r.a && l.b == r.b;
}

std::ostream & operator << (std::ostream & o , const MyStruct & s)
{
    return o << "{a: " << s.a << " , b: "<<s.b << "}";
}

MATCHER(MyStructMatcher, "")
{
    const MyStruct & l = std::get<0>(arg);
    const MyStruct & r = std::get<1>(arg);
    EXPECT_EQ(l.a, r.a);
    EXPECT_EQ(l.b, r.b);
    return l.a == r.a && l.b == r.b;
}

MATCHER(MyStructMatcherWithField, "")
{
    const MyStruct & l = std::get<0>(arg);
    const MyStruct & r = std::get<1>(arg);

    return ExplainMatchResult(AllOf(
            Field(&MyStruct::a, r.a),
            Field(&MyStruct::b, r.b)), l , result_listener);
}



MATCHER_P2(MyStructSingleMatcherWithField, a_, b_, "is an MyStruct with fields: {a :"+ std::to_string(a_) + ", b:" +  std::to_string(b_)+ "}")
{
    return ExplainMatchResult(
            AllOf(
                    Field(&MyStruct::a, a_),
                    Field(&MyStruct::b, b_)
            ), arg, result_listener);
}


TEST(Example, Ex_Pointwise)
{
    std::vector<MyStruct> l {{1,0.3}, {2,0.4}, {0,0.2}};
    std::vector<MyStruct> r {{5,0.3}, {2,0.2}, {0,0.2}};


    std::cout<< " Z operatorem == "<<std::endl;
    std::cout<< " EXPECT_EQ"<<std::endl;
    EXPECT_EQ(l,r);
    std::cout<<std::endl<<"ContainerEq"<<std::endl;
    EXPECT_THAT(r, ContainerEq(l));


    std::cout<<std::endl << " Bez operatora =="<<std::endl;
    std::cout<< " EXPECT_EQ"<<std::endl;

    ASSERT_EQ(l.size(), r.size());
    for (int i = 0; i < l.size(); ++i)
    {
        EXPECT_EQ(l[i].a, r[i].a);
        EXPECT_EQ(l[i].b, r[i].b);
    }

    std::cout<<std::endl<< " Pointwise z Matcherem"<<std::endl;
    EXPECT_THAT(r, Pointwise(MyStructMatcher(), l));

    std::cout<<std::endl<< " Pointwise z Matcherem z field"<<std::endl;
    EXPECT_THAT(r, Pointwise(MyStructMatcherWithField(), l));

    std::cout<<std::endl<< " Elements Are"<<std::endl;
    EXPECT_THAT(r, ElementsAre(
            MyStructSingleMatcherWithField( 5, 0.3),
            MyStructSingleMatcherWithField( 2, 0.4),
            MyStructSingleMatcherWithField( 0, 0.2)
    ));

}
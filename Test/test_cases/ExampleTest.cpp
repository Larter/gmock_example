#include <memory>
#include "gmock/gmock.h"
#include "MoView.h"
#include "AmazonStorage.h"

using namespace testing;

typedef  int Dobry_Int;


enum class MojEnum{
    one = 2,
    two = 3
};

struct MyStruct {
    Dobry_Int pole;
    bool cos;
    MojEnum enumm;
};



TEST(EXAMPLE, shallBeExample) {

    MyStruct* moje = new MyStruct();
    moje->cos = true;
    moje->pole =2;
    moje->enumm = MojEnum::one;
    std::string nazwa = "nazwa";
    MoView<MyStruct> moje_mo_view(&nazwa, moje);
    EXPECT_THAT(moje_mo_view, Pointee(Field(&MyStruct::pole, 2)));

    std::cout<<"Dzialam" <<std::endl;
}

